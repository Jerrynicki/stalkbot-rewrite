import discord
import discord.ext.commands as commands
import asyncio
import time

class Cfg(commands.Cog):
	def __init__(self, bot, config, features_toggle, functions, timeouts, command_log):
		self.bot = bot
		self.config = config
		self.features_toggle = features_toggle
		self.functions = functions
		self.timeouts = timeouts
		self.command_log = command_log

	@commands.command(aliases=["config"])
	async def cfg(self, ctx):
		if self.timeouts.is_timeout("cfg"):
			await ctx.message.add_reaction(self.bot.emoji.hourglass)
			return
		else:
			self.timeouts.add("cfg", self.config["timeout"])

		try:
			self.command_log.append((time.time(), ctx, "Config"))

			max_dist = max([len(x) for x in self.config])

			message = "```"
			for x in self.config:
				if x != "token":
					message += x + ": " + " "*(max_dist - len(x)) + str(self.config[x]) + "\n"
			message += "```"

			await ctx.send(message)

		except Exception as exc:
			await ctx.message.add_reaction(self.bot.emoji.cross_mark)
			await ctx.send("Error! " + str(type(exc)) + " " + str(exc))
