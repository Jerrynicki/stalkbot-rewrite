import discord
import discord.ext.commands as commands
import asyncio
import os
import time
import requests

class Play(commands.Cog):
	def __init__(self, bot, config, features_toggle, functions, timeouts, command_log):
		self.bot = bot
		self.config = config
		self.features_toggle = features_toggle
		self.functions = functions
		self.timeouts = timeouts
		self.command_log = command_log

	@commands.command()
	async def play(self, ctx, *args):
		"""play the funny
		attach a file or a youtube-dl supported link
		if you want to start at a specific timestamp (in seconds), put it there and stuff
		yeah
		also you do the prefix!play youtube.com/amogus 15 to start at 15s
		or prefix!play 15 and attach a file to start the file at yeah you get the idea
		jrery out"""

		if self.timeouts.is_timeout("play"):
			await ctx.message.add_reaction(self.bot.emoji.hourglass)
			return
		else:
			self.timeouts.add("play", self.config["timeout"])
		
		if not self.features_toggle["play"]:
			await ctx.message.add_reaction(self.bot.emoji.no_bell)
			return
		
		try:
			url = None
			start_point = 0

			if len(args) >= 1:
				if args[0].startswith("http"):
					if "list=" in args[0]:
						await ctx.send("hört auf dauernd playlist links zu senden wat soll dit")
						return
						
					url = args[0]
					try:
						start_point = int(args[1])
					except (ValueError, IndexError):
						pass
				else:
					try:
						start_point = int(args[0])
					except (ValueError, IndexError):
						pass
						
			if len(ctx.message.attachments) == 0 and url == None:
				await ctx.send("wo is DA FILY <a:modCheck:765305750293053470> WAT IS DIT?, " + ctx.message.author.mention + "?")
				return
			
			self.functions.notification(self.config["notifications_format"], "Play file", ctx)
			await self.functions.warning_sound()
			
			if url == None:
				self.command_log.append((time.time(), ctx, "Play: " + ctx.message.attachments[0].filename))
			else:
				self.command_log.append((time.time(), ctx, "Play: " + args[0].split("/")[-1]))
			await ctx.message.add_reaction(self.bot.emoji.inbox_tray)

			if url == None:
				filename = "cache/play." + ctx.message.attachments[0].filename.split(".")[-1]
				await ctx.message.attachments[0].save(filename)
			else:
				result = self.functions.ytdl(url)
				
				if result is False:
					raise Exception("it brokey (also ich mein ytdl)")
					
				files = os.listdir("cache")
				for x in files:
					if x.startswith("play."):
						filename = "cache/"+x
						break
			
			result = self.functions.ffmpeg(filename, ["-ss", str(start_point), "-af", "volume=-25dB,loudnorm=tp=0", "-t", \
										   str(self.config["max_message_length"]), "-ar", "44100", "-ac", "2"], filename + "_converted.wav")

			if result is False:
				raise Exception("FFmpeg command timed out or returned an error")
			
			await ctx.message.remove_reaction(self.bot.emoji.inbox_tray, ctx.message.guild.me)
			
			await ctx.message.add_reaction(self.bot.emoji.play)
			
			await self.functions.play_sound(filename + "_converted.wav")
			await ctx.message.remove_reaction(self.bot.emoji.play, ctx.message.guild.me)
			await ctx.message.add_reaction(self.bot.emoji.check_mark)
			
			os.unlink(filename)
			os.unlink(filename + "_converted.wav")
			
		except Exception as exc:
			raise exc
			await ctx.message.add_reaction(self.bot.emoji.cross_mark)
			await ctx.send("Error! " + str(type(exc)) + " " + str(exc))
