import discord
import discord.ext.commands as commands
import asyncio
import time
import os
import threading
import subprocess

# pactl list short sources

class Microphone(commands.Cog):
	def __init__(self, bot, config, features_toggle, functions, timeouts, command_log):
		self.bot = bot
		self.config = config
		self.features_toggle = features_toggle
		self.functions = functions
		self.timeouts = timeouts
		self.command_log = command_log

	@commands.command(aliases=["mic"])
	async def microphone(self, ctx, speed=1.0):
		if self.timeouts.is_timeout("microphone"):
			await ctx.message.add_reaction(self.bot.emoji.hourglass)
			return
		else:
			self.timeouts.add("microphone", self.config["timeout"])

		if not self.features_toggle["microphone"]:
			await ctx.message.add_reaction(self.bot.emoji.no_bell)
			return

		if speed < 0.25 or speed > 10:
			await ctx.send("no")
			return

		try:
			self.functions.notification(self.config["notifications_format"], "Microphone", ctx)
			await self.functions.warning_sound()
			self.command_log.append((time.time(), ctx, "Microphone"))
			
			await ctx.message.add_reaction(self.bot.emoji.microphone)

			await self.functions.play_sound("mic_record.wav")
			
			subprocess.run(["ffmpeg", "-f", "pulse", "-i", self.config["mic_input"], "-ss", "00:00:00.2", "-t", \
			str((self.config["rec_length"]+0.2)/speed), "-af", "asetrate=48000*"+str(speed), "-ar", "48000", "-ac", "1", "-b:a",\
			"96k", "-c:a", "libopus", "cache/mic.ogg"], check=True, timeout=20)
			await self.functions.play_sound("mic_record.wav")
		
			await ctx.send("funny", file=discord.File("cache/mic.ogg"))
			await ctx.message.remove_reaction(self.bot.emoji.microphone, ctx.message.guild.me)
			await ctx.message.add_reaction(self.bot.emoji.check_mark)
			
			os.unlink("cache/mic.ogg")

		except Exception as exc:
			await ctx.message.add_reaction(self.bot.emoji.cross_mark)
			await ctx.send("Error! " + str(type(exc)) + " " + str(exc))
			raise exc

	@commands.command(aliases=["out"])
	async def output(self, ctx):
		if self.timeouts.is_timeout("output"):
			await ctx.message.add_reaction(self.bot.emoji.hourglass)
			return
		else:
			self.timeouts.add("output", self.config["timeout"])

		if not self.features_toggle["output"]:
			await ctx.message.add_reaction(self.bot.emoji.no_bell)
			return

		try:
			self.functions.notification(self.config["notifications_format"], "Output", ctx)
			await self.functions.warning_sound()
			self.command_log.append((time.time(), ctx, "Output"))
			
			await ctx.message.add_reaction(self.bot.emoji.microphone)

			await self.functions.play_sound("mic_record.wav")
			
			subprocess.run(["ffmpeg", "-f", "pulse", "-i", self.config["out_input"], "-ss", "00:00:00.2", "-t", \
			str(self.config["rec_length"]), "-ar", "48000", "-ac", "2", "-b:a",\
			"96k", "-c:a", "libopus", "cache/out.ogg"], check=True, timeout=20)
			await self.functions.play_sound("mic_record.wav")
		
			await ctx.send("funny", file=discord.File("cache/out.ogg"))
			await ctx.message.remove_reaction(self.bot.emoji.microphone, ctx.message.guild.me)
			await ctx.message.add_reaction(self.bot.emoji.check_mark)
			
			os.unlink("cache/out.ogg")

		except Exception as exc:
			await ctx.message.add_reaction(self.bot.emoji.cross_mark)
			await ctx.send("Error! " + str(type(exc)) + " " + str(exc))
			raise exc
