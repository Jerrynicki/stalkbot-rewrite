import tkinter as tk
import requests
import json
import hashlib
import os
import webbrowser
import threading

# 15271852
# response = requests.get("https://gitlab.com/api/v4/projects/15271852/repository/files/main.py?ref=master").json()

def open_website(root):
	webbrowser.open("https://gitlab.com/Jerrynicki/stalkbot-rewrite")
	root.quit()

def check_for_new_version_bg():
	threading.Thread(target=check_for_new_version).start()

def check_for_new_version():
	print("Checking for updates...")

	hashes_local = list()
	hashes_remote = list()

	files = ("main.py", *["commands/"+x for x in os.listdir("commands")], *["utils/"+x for x in os.listdir("utils")])

	for file in files:
		if not file.endswith(".py"):
			continue

		print("Hash for " + file + "...", end="")
		hashes_local.append(hashlib.sha256(open(file, "rb").read()).hexdigest())
		try:
			response = requests.get("https://gitlab.com/api/v4/projects/15271852/repository/files/" + file.replace("/", "%2F").replace("\\", "%2F") + "?ref=master").json()
			hashes_remote.append(response["content_sha256"])
		except:
			hashes_remote.append("?"*64)
			
		print("Local: " + hashes_local[-1] + " | Remote: " + hashes_remote[-1])

	sha_local = hashlib.sha256("".join(hashes_local).encode("ascii")).hexdigest()
	sha_remote = hashlib.sha256("".join(hashes_remote).encode("ascii")).hexdigest()

	if sha_remote != sha_local:
		print("out of date? " + sha_local + "!=" + sha_remote)

		root = tk.Tk()
		root.title("Stalkbot Updater")
		
		message = tk.Label(root, text="Hash mismatch!\nThis means there is probably a new version available!", font=("Helvetica", 18))

		button_openbrowser = tk.Button(root, text="Open browser to download new version", font=("Helvetica", 12), command=lambda root=root: open_website(root))
		button_ignore = tk.Button(root, text="ok cool idc and i also smell bad", font=("Helvetica", 12), command=root.quit)

		message.pack()
		button_openbrowser.pack()
		button_ignore.pack()
		
		root.mainloop()
		root.destroy()
	else:
		print("up to date")
