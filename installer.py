import json
import os

print("Welcome to the Stalkbot installer!\n")

platform = os.name
if platform == "nt":
	print("Your system has been detected as Windows. If this is correct, press enter to continue.")
	input()
elif platform == "posix":
	print("Your system has been detected as POSIX (Linux/macOS/BSD/etc.). If this is correct, press enter to continue.")
	input()
else:
	print("Your system could not be detected. Please enter it manually ('nt' or 'posix')")
	platform = input("> ")

if platform == "nt":	
	print("Installing required pip packages...")
	os.system("py -m pip install pygame psutil discord.py Pillow gTTS win10toast requests opencv-python --user")
elif platform == "posix":
	print("""First, please make sure the following packages are installed on your system and on your PATH: 
  "scrot"
  "notify-send / libnotify"
  "pip3 / python3-pip"
  "Tkinter for Python 3 / python3-tk"
  "ffmpeg"
  "libsdl1.2-dev"
Please note that they may be named differently on your distribution""")
	input()
	print("Installing required pip packages...")
	os.system("pip3 install pygame psutil discord.py Pillow gTTS requests")

input("Press enter if the installation was successful.")

print("""Now to set up your bot config.
The options will be written to the file config.json. You can change them later.

All time values are in seconds.""")

# token cam_width cam_height webcam_delay screenshot_blur timeout prefix tts_voice max_message_length status notifications_format

token = input("Please input your bot token (you can get one by creating a bot account at https://discordapp.com/developers): ")
print("If you do not know your webcam width/height, just enter 0 for the following values and the default 320x240 will be used.")
cam_width = int(input("Please input the width of your webcam (px): "))
cam_height = int(input("Please input the height of your webcam (px): "))

print("""Please input a smaller width and height of your webcam to be used for the webcam gif command,
since the images will be downscaled anyway (for a 3s gif the width will usually be around 400)
and most webcams support a higher framerate at lower resolutions
Like the normal width and height, you can set this to 0 and the bot will use a default of 320x240""")

small_cam_width = int(input("Smaller width (px): "))
small_cam_height = int(input("Smaller height (px): "))

webcam_delay = float(input("Please input the desired delay before taking a picture when the webcam command is used: "))
rec_length = float(input("Please input the maximum length when recording a webcam video, microphone input or audio output (seconds): "))
screenshot_blur = float(input("Please input the desired amount of blur when taking screenshots: "))
timeout = float(input("Please input the cooldown/timeout for commands: "))
prefix = input("Please input your desired bot prefix: ")
tts_voice = input("Please input the text-to-speech voice to use (Google TTS, e.g. 'en' for English, 'de' for German): ")
max_message_length = float(input("Please input the maximum length for tts/play commands: "))
status = input("Please input a status for the bot to be broadcasting (Playing xyz) (leave empty for none): ")

print("""


Specific strings in the notifications format will be replaced by values
COMMAND will be replaced by the command
AUTHOR will be replaced by the person who invoked the command
SERVER will be replaced by the server the command was invoked on
CHANNEL will be replaced by the channel the command was invoked in
An example format would be:
AUTHOR: COMMAND | SERVER CHANNEL
Which would produce the following output when Flexis sends a screenshot command in #bots on Supermarkt:
Flexis#1234: Screenshot | Supermarkt #bots""")

notifications_format = input("Please input the desired format for notifications: ")
folder = input("Please input a folder for the folder command: ")

if platform == "posix":
	os.system("pactl list short sources")
	mic_input = input("Please choose an audio device from this list to record microphone input: ")
	out_input = input("Please also choose a monitor device from this list to record audio output: ")
else:
	print("Audio input and output recording are currently not supported on your system. If they are in the future, simply input the correct values for mic_input and out_input in the config editor or run this installer again.")

check_updates = None
while check_updates not in ("y", "n"):
	check_updates = input("Would you like the bot to check for updates on each start? (y/n")
	
check_updates = {"y": True, "n": False}[check_updates]

config = {"token": token, "cam_width": cam_width, "cam_height": cam_height, "small_cam_width": small_cam_width, "small_cam_height": small_cam_height, "webcam_delay": webcam_delay, "screenshot_blur": screenshot_blur, "timeout": timeout, "prefix": prefix, "tts_voice": tts_voice, "max_message_length": max_message_length, "status": status, "notifications_format": notifications_format, "folder": folder, "rec_length": rec_length, "mic_input": mic_input, "out_input": out_input, "check_updates": check_updates}
json.dump(config, open("config.json", "w"))

print("Success! You can now run the bot.")
